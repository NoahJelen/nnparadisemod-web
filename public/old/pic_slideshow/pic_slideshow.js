var category = getUrlVars()["cat"];
const next_link = "<a href=\"javascript:nextImage()\">Next</a>";
const prev_link = "<a href=\"javascript:prevImage()\">Previous</a>";
var path = "../categories/world/biomes/";
var images = ["rocky_desert.png","cold_rocky_desert.png","glacier.png","salt_flat.png","snowy_rocky_desert.png","temperate_rainforest.png","volcanic_field.png","volcanic_mountains.png"];
var titles = ["Rocky Desert","Cold Rocky Desert","Glacier","Salt Flat","Snowy Rocky Desert","Temperate Jungle","Volcanic Field","Volcanic Mountains"];
switch (category) {
    case "biomes":
        images = ["rocky_desert.png","cold_rocky_desert.png","glacier.png","salt_flat.png","snowy_rocky_desert.png","temperate_rainforest.png","volcanic_field.png","volcanic_mountains.png"];
        path = "../categories/world/biomes/";
        titles = ["Rocky Desert","Cold Rocky Desert","Glacier","Salt Flat","Snowy Rocky Desert","Temperate Jungle","Volcanic Field","Volcanic Mountains"];
        break;
    case "underground":
        images = ["cave_crystal.png","cave_desert.png","cave_icy_desert.png","cave_icy.png","cave_jungle.png","cave_mesa.png","cave_normal.png","cave_ocean.png","cave_rocky.png","cave_swamp.png","salt_cave.png"];
        path = "../categories/world/underground/";
        titles = ["Crystal Cave","Desert Cave","Frozen Desert Cave","Icy Cave","Lush Cave","Mesa Cave","Normal Cave","Ocean Cave","Rocky Desert Cave","Slimy Cave","Salt Cave"]
        break;
    case "dungeons":
        images = ["brick_pyramid_inside.png","brick_pyramid.png","large_void_dungeon_inside.png","large_void_dungeon.png","little_void_dungeon.png","medium_void_dungeon.png","mesa_temple.png","minerbase.png","mini_stringhold.png","shipwreck.png","sky_wheel.png","void_tower.png"];
        path = "../categories/world/dungeons/";
        titles = ["Brick Pyramid (inside)","Brick Pyramid","Large Void Dungeon (inside)","Large Void Dungeon","Little Void Dungeon","Medium Void Dungeon","Mesa Temple","Abandoned Mining Base","Mini Stronghold","Shipwreck","Sky Wheel","Void Tower"];
        break;
    case "houses":
        images = ["base_glacier.png","base_obsidian.png","base_salt.png","home_acacia.png","home_bigoak.png","home_birch.png","home_cactus.png","home_jungle.png","home_spruce.png","home.png"];
        path = "../categories/world/homes/";
        titles = ["Glacier Base","Volcanic Base","Salt Flat Base","Savanna House","Roofed Forest House","Birch Home","Cactus Home","Jungle Home","Spruce Home","Oak Home"];
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function nextImage() {
    var num_images = images.length - 1;
    var curr_img = parseInt(document.getElementById("image").getAttribute("data-num"));

    if (curr_img >= num_images) {
        document.getElementById("image").setAttribute("src", path + images[0]);
        document.getElementById("image").setAttribute("data-num",0);
        document.getElementById("title").innerHTML = prev_link + " " + titles[0] + " " + next_link;
    }
    else {
        document.getElementById("image").setAttribute("src", path + images[curr_img + 1]);
        document.getElementById("image").setAttribute("data-num",curr_img + 1);
        document.getElementById("title").innerHTML = prev_link + " " + titles[curr_img + 1]  + " " + next_link;
    }
}


function prevImage() {
    var num_images = images.length - 1;
    var curr_img = parseInt(document.getElementById("image").getAttribute("data-num"));

    if (curr_img <= 0) {
        document.getElementById("image").setAttribute("src", path + images[num_images]);
        document.getElementById("image").setAttribute("data-num",num_images);
        document.getElementById("title").innerHTML = prev_link + " " +  titles[num_images]  + " " + next_link;
    }
    else {
        document.getElementById("image").setAttribute("src", path + images[curr_img - 1]);
        document.getElementById("image").setAttribute("data-num",curr_img - 1);
        document.getElementById("title").innerHTML = prev_link + " " +  titles[curr_img - 1]  + " " + next_link;
    }
}