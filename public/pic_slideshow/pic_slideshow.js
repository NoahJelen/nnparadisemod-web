class PicSlideShow{
    constructor(path){
        this.path=path;
        this.images=[];
    }
    image(image,title){
        this.images.push({
            image:image,
            title:title
        });
        return this;
    }
    cur_image(){
        var cur_index=parseInt(document.getElementById("image").getAttribute("data-num"));
        return this.path+this.images[cur_index].image;
    }
    cur_title(){
        var cur_index=parseInt(document.getElementById("image").getAttribute("data-num"));
        return this.images[cur_index].title;
    }
    next_image(){
        var image=document.getElementById("image");
        var num_images=this.images.length-1;
        var cur_index=parseInt(image.getAttribute("data-num"));
        if(cur_index>=num_images){
            image.setAttribute("src",this.path+this.images[0].image);
            image.setAttribute("data-num",0);
            image.setAttribute("alt",this.images[0].title);
            document.getElementById("title").innerHTML=this.images[0].title;
        }
        else{
            image.setAttribute("src",this.path+this.images[cur_index+1].image);
            image.setAttribute("data-num",cur_index+1);
            image.setAttribute("alt",this.images[cur_index+1].title);
            document.getElementById("title").innerHTML=this.images[cur_index+1].title;
        }
    }
    prev_image(){
        var image=document.getElementById("image");
        var num_images=this.images.length-1;
        var cur_index=parseInt(image.getAttribute("data-num"));
        if(cur_index<=0){
            image.setAttribute("src",this.path+this.images[num_images].image);
            image.setAttribute("data-num",num_images);
            image.setAttribute("alt",this.images[num_images].title);
            document.getElementById("title").innerHTML=this.images[num_images].title;
        }
        else{
            image.setAttribute("src",this.path+this.images[cur_index-1].image);
            image.setAttribute("data-num",cur_index-1);
            image.setAttribute("alt",this.images[cur_index - 1].title);
            document.getElementById("title").innerHTML=this.images[cur_index - 1].title;
        }
    }
}
var category=getUrlVars()["cat"];
var slideshow;
switch (category){
    case "biomes":
        slideshow=new PicSlideShow("../world/biomes/")
            .image("autumn_forest.png","Autumn Forest")
            .image("glacier.png","Glacier")
            .image("rocky_desert.png","Rocky Desert")
            .image("mesquite_forest.png","Mesquite Forest")
            .image("palo_verde_forest.png","Palo Verde Forest")
            .image("high_rocky_desert.png","High Rocky Desert")
            .image("snowy_rocky_desert.png","Snowy Rocky Desert")
            .image("rose_field.png","Rose Field")
            .image("salt_flat.png","Salt Flat")
            .image("temperate_rainforest.png","Temperate Rainforest")
            .image("the_origin.png","The Origin")
            .image("volcanic_field.png","Volcanic Field")
            .image("subglacial_volcanic_field.png","Subglacial Volcanic Field")
            .image("weeping_forest.png","Weeping Forest")
            .image("honey_cave.png","Honey Cave")
            .image("concrete_badlands.png","Concrete Badlands")
            .image("christmas_forest.png","Christmas Forest")
            .image("deep_dark_flats.png","Deep Dark Flats")
            .image("frozen_deep_dark_flats.png","Frozen Deep Dark Flats")
            .image("glowing_forest.png","Glowing Forest")
            .image("crystal_forest.png","Crystal Forest")
            .image("glowing_glacier.png","Glowing Glacier")
            .image("polar_winter.png","Polar Winter")
            .image("silva_insomnium.png","Silva Insomnium")
            .image("taiga_insomnium.png","Taiga Insomnium");
        break;
    case "underground":
        slideshow=new PicSlideShow("../world/underground/")
            .image("cave_normal.png","Normal Cave")
            .image("cave_badlands.png","Badlands Cave")
            .image("cave_desert.png","Desert Cave")
            .image("cave_rocky_desert.png","Rocky Desert Cave")
            .image("salt_cave.png","Salt Cave")
            .image("cave_icy.png","Icy Cave")
            .image("cave_jungle.png","Jungle Cave")
            .image("cave_swamp.png","Swamp Cave")
            .image("cave_ocean.png","Ocean Cave");
        break;
    case "dungeons":
        slideshow=new PicSlideShow("../world/dungeons/")
            .image("badlands_pyramid.png","Badlands Pyramid")
            .image("brick_pyramid.png","Brick Pyramid")
            .image("brick_pyramid_inside.png","Brick Pyramid (inside)")
            .image("dark_tower.png","Dark Tower")
            .image("large_dark_dungeon_inside.png","Large Dark Dungeon (inside)")
            .image("large_dark_dungeon.png","Large Dark Dungeon")
            .image("medium_dark_dungeon.png","Medium Dark Dungeon")
            .image("minerbase.png","Abandoned Mining Base")
            .image("sky_wheel.png","Wheel in the Sky")
            .image("small_dark_dungeon.png","Small Dark Dungeon")
            .image("small_stronghold.png","Small Stronghold")
            .image("ender_outpost.png","Ender Outpost");
        break;
    case "houses":
        slideshow=new PicSlideShow("../world/homes/")
            .image("base_glacier.png","Glacier Research Base")
            .image("base_obsidian.png","Obsidian Research Base")
            .image("base_salt.png","Salt Research Base")
            .image("home_acacia.png","Acacia Home")
            .image("home_birch.png","Birch Home")
            .image("home_cactus.png","Cactus Home")
            .image("home_darkoak.png","Dark Oak Home")
            .image("home_jungle.png","Jungle Home")
            .image("home_mesquite.png","Mesquite Home")
            .image("home_palo_verde.png","Palo Verde Home")
            .image("home_spruce.png","Spruce Home")
            .image("home.png","Oak Home");
        break;
    case "structures":
        slideshow=new PicSlideShow("../world/structures/")
            .image("buoy.png","Rusted Iron Buoy")
            .image("runway.png","Elytra Runway")
            .image("small_buoy.png","Lantern Buoy")
            .image("terrarium.png","Terrarium")
            .image("trader_tent.png","Wandering Trader Tent")
            .image("wicker_man.png","Wicker Man Monument")
            .image("rogue_end_spike.png","Rogue End Spike");
}
function getUrlVars(){
    var vars={};
    var parts=window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(m,key,value){
        vars[key]=value;
    });
    return vars;
}