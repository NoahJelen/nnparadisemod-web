let IMAGES=[
    '/world/biomes/autumn_forest.png',
    '/world/biomes/glacier.png',
    '/world/biomes/rocky_desert.png',
    '/world/biomes/mesquite_forest.png',
    '/world/biomes/palo_verde_forest.png',
    '/world/biomes/high_rocky_desert.png',
    '/world/biomes/snowy_rocky_desert.png',
    '/world/biomes/rose_field.png',
    '/world/biomes/salt_flat.png',
    '/world/biomes/temperate_rainforest.png',
    '/world/biomes/the_origin.png',
    '/world/biomes/volcanic_field.png',
    '/world/biomes/weeping_forest.png',
    '/world/biomes/honey_cave.png',
    '/world/biomes/concrete_badlands.png',
    '/world/biomes/christmas_forest.png',
    '/world/biomes/deep_dark_flats.png',
    '/world/biomes/frozen_deep_dark_flats.png',
    '/world/biomes/glowing_forest.png',
    '/world/biomes/crystal_forest.png',
    '/world/biomes/glowing_glacier.png',
    '/world/biomes/polar_winter.png',
    '/world/biomes/silva_insomnium.png',
    '/world/biomes/taiga_insomnium.png',
    '/world/biomes/subglacial_volcanic_field.png',
    '/world/dimensions/overworld_core.png',
    '/world/dimensions/deep_dark.png',
    '/world/dimensions/elysium.png',
    '/world/underground/cave_desert.png',
    '/world/underground/cave_icy.png',
    '/world/underground/cave_jungle.png',
    '/world/underground/cave_badlands.png',
    '/world/underground/cave_normal.png',
    '/world/underground/cave_ocean.png',
    '/world/underground/cave_rocky_desert.png',
    '/world/underground/cave_swamp.png',
    '/world/underground/salt_cave.png'
];
function genUi(wiki = false) {
    var navbar = new NavbarBuilder();
    loadStyleSheet((wiki?"https://www.paradisemod.net":"")+"/styles.css");
    loadStyleSheet("https://www.aercloud-systems.net/aercloud.css");
    if(wiki){
        setIcon("https://www.paradisemod.net/favicon.png");
        setTitle("Paradise Mod Wiki");
        navbar.link('<img src="https://www.paradisemod.net/favicon.png" width="16px" height="16px">Main Site',"https://www.paradisemod.net")
            .link("Home","/")
            .link('<img src="https://www.paradisemod.net/curseforge.png">Download',"https://www.curseforge.com/minecraft/mc-mods/paradise-mod")
            .link('<img src="https://www.paradisemod.net/modrinth.png">Download',"https://modrinth.com/mod/paradise-mod")
            .link('<img src="https://www.paradisemod.net/gitlab.png">GitLab',"https://gitlab.com/NoahJelen/paradisemod");
    }
    else{
        navbar.text('<img id="versions" src="https://img.shields.io/badge/Available%20For-1.18.2%20|%201.16.5%20-green">')
            .link('<img src="/curseforge.png">Download',"https://www.curseforge.com/minecraft/mc-mods/paradise-mod")
            .link('<img src="/modrinth.png">Download',"https://modrinth.com/mod/paradise-mod")
            .link('<img src="/gitlab.png">GitLab',"https://gitlab.com/NoahJelen/paradisemod")
            .link("Wiki","https://wiki.paradisemod.net")
            .link("Companion Mods","/companions/")
            .link("Old Site","/old/")
            .newLine()
            .link("Home","/")
            .link("Automation","/automation/")
            .link("Building","/building/")
            .link("Decoration","/decoration/")
            .link("Miscellaneous","/miscellaneous/")
            .link("Redstone","/redstone/")
            .link("World","/world/")
            .link("Bonus Features","/bonus/");
        setIcon("/favicon.png");
        setTitle("Paradise Mod");
    }
    addFooter('Copyright hahaha  Mod Problems/Minecraft crash? <a href="https://gitlab.com/NoahJelen/paradisemod/-/issues">Click Here!</a>');
    navbar.build();
    IMAGES.forEach((part,index,images)=>{
        if(wiki) images[index]="https://www.paradisemod.net"+part;
    });
    addBanner("/logo.png",IMAGES,wiki?13:11);
    setBackgroundImage(wiki?"https://www.paradisemod.net/background.png":"/background.png");
}